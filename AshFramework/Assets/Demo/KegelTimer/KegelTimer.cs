using System;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using TMPro;
using UnityEngine;
using UnityEngine.Serialization;
using UnityEngine.UI;

public class KegelTimer : MonoBehaviour
{
    public Text timerText; // UI文本组件，用于显示时间
    public Text totalText; // UI文本组件，用于显示时间
    public Slider slider; // UI文本组件，用于显示时间
    public List<AudioClip> audioClips = new List<AudioClip>();

    //SettingPanel
    public Transform settingPanel;
    public Text infoText;
    public TMP_InputField inputField;

    private int _duration = 10; // 计时器的总时长（秒）
    private float _timeRemaining = 0; // 剩余时间
    private int _total = 0;
    private bool _isRunning = false; // 计时器状态
    private AudioSource _audioSource;

    private void Start()
    {
        _audioSource = transform.GetComponent<AudioSource>();

        _duration = PlayerPrefs.GetInt("KegelDuration", 10);

        // 初始化剩余时间
        slider.value = _timeRemaining / _duration;
        totalText.text = $"{_total}";
        UpdateTimerText();
    }

    private void Update()
    {
        // 检查计时器是否在运行
        if (_isRunning)
        {
            // 更新剩余时间
            _timeRemaining -= Time.deltaTime;

            // 如果剩余时间小于等于零，停止计时器
            if (_timeRemaining <= 0)
            {
                _timeRemaining = 0;
                _isRunning = false;
                StopTimer();
                AddTodayTotalText();
            }

            slider.value = _timeRemaining / _duration;

            // 更新UI文本
            UpdateTimerText();
        }
    }

    // 启动计时器
    public void StartTimer()
    {
        _audioSource.PlayOneShot(audioClips[0]);
        _isRunning = true;
    }

    // 停止计时器
    public void StopTimer()
    {
        _audioSource.PlayOneShot(audioClips[1]);
        _isRunning = false;
    }

    // 重置计时器
    public void ResetTimer()
    {
        _timeRemaining = _duration;
        _isRunning = false;
        UpdateTimerText();
    }

    // 更新UI显示的时间
    private void UpdateTimerText()
    {
        if (timerText != null)
        {
            timerText.text = _timeRemaining.ToString("F1"); // 保留两位小数
        }
    }

    private void AddTodayTotalText()
    {
        _total++;
        totalText.text = $"{_total}";
        if (_total == 10)
        {
            AddTodayGroupCount();
        }

        AddTotalCountText();
    }

    private void AddTotalCountText()
    {
        var count = PlayerPrefs.GetInt("KegelTotalCount", 0);
        count++;
        PlayerPrefs.SetInt("KegelTotalCount", count);
    }

    public void OnAddButtonClick()
    {
        if (_timeRemaining > 0) return;
        ResetTimer();
        StartTimer();
    }

    private void AddTodayGroupCount()
    {
        var count = PlayerPrefs.GetInt($"KegelTodayGroupCount_{DateTime.Now.ToString("yyyy-MM-dd")}", 0);
        count++;
        PlayerPrefs.SetInt($"KegelTodayGroupCount_{DateTime.Now.ToString("yyyy-MM-dd")}", count);
        if (count == 2)
        {
            var daysCount = PlayerPrefs.GetInt("KegelDays", 0);
            daysCount++;
            PlayerPrefs.SetInt("KegelDays", daysCount);
        }
    }

    private void ShowSettingPanel(bool show)
    {
        settingPanel.gameObject.SetActive(show);

        if (show)
        {
            infoText.text = "";
            inputField.text = $"{_duration}";
            var info =
                $"当前次数：{_total}\t\t" +
                $"今日组次：{PlayerPrefs.GetInt($"KegelTodayGroupCount_{DateTime.Now.ToString("yyyy-MM-dd")}", 0)}\n" +
                $"总次数：{PlayerPrefs.GetInt("KegelTotalCount", 0)}\n" +
                $"天数：{PlayerPrefs.GetInt("KegelDays", 0)}";
            infoText.DOText(info, 1f).SetEase(Ease.OutBack);
        }
        else
        {
            if (!string.IsNullOrEmpty(inputField.text))
            {
                _duration = int.Parse(inputField.text);
                PlayerPrefs.SetInt("KegelDuration", _duration);
            }
        }
    }

    public void OnShowSetttingPanelButtonClick()
    {
        ShowSettingPanel(true);
    }

    public void OnSetttingPanelCloseButtonClick()
    {
        ShowSettingPanel(false);
    }
}