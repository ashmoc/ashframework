using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//1.寻找数组的中心索引
//2.搜索插入位置
//3.合并区间

public class Code_1_0 : MonoBehaviour
{
    void Start()
    {
        // // 示例1
        // // 输入：nums = [1, 7, 3, 6, 5, 6]
        // // 输出：3
        // // 解释：
        // // 中心下标是 3 。
        // // 左侧数之和 sum = nums[0] + nums[1] + nums[2] = 1 + 7 + 3 = 11 ，
        // // 右侧数之和 sum = nums[4] + nums[5] = 5 + 6 = 11 ，二者相等。
        // Debug.Log(Example_1(new List<int>() { 1, 7, 3, 6, 5, 6 }));
        //
        // // 示例2
        // // 输入：nums = [1, 2, 3]
        // // 输出：-1
        // // 解释：
        // // 数组中不存在满足此条件的中心下标。
        // Debug.Log(Example_1(new List<int>() { 1, 2, 3 }));
        //
        // // 示例3
        // // 输入：nums = [2, 1, -1]
        // // 输出：0
        // // 解释：
        // // 中心下标是 0 。
        // // 左侧数之和 sum = 0 ，（下标 0 左侧不存在元素），
        // // 右侧数之和 sum = nums[1] + nums[2] = 1 + -1 = 0 。
        // Debug.Log(Example_1(new List<int>() { 2, 1, -1 }));

        // // 示例1
        // // 输入: nums = [1,3,5,6], target = 5
        // // 输出: 2
        // Debug.Log(Example_2(new List<int>() { 1, 3, 5, 6 }, 5));
        //
        // // 示例1
        // // 输入: nums = [1,3,5,6], target = 2
        // // 输出: 1
        // Debug.Log(Example_2(new List<int>() { 1, 3, 5, 6 }, 2));
        //
        // // 示例1
        // // 输入: nums = [1,3,5,6], target = 7
        // // 输出: 4
        // Debug.Log(Example_2(new List<int>() { 1, 3, 5, 6 }, 7));
    }

    // 题目1
    // 给你一个整数数组 nums ，请计算数组的 中心下标 。
    // 数组 中心下标 是数组的一个下标，其左侧所有元素相加的和等于右侧所有元素相加的和。
    // 如果中心下标位于数组最左端，那么左侧数之和视为 0 ，因为在下标的左侧不存在元素。这一点对于中心下标位于数组最右端同样适用。
    // 如果数组有多个中心下标，应该返回 最靠近左边 的那一个。如果数组不存在中心下标，返回 -1 。

    int Example_1(List<int> nums)
    {
        int sum = 0;
        for (int i = 0; i < nums.Count; i++)
        {
            sum += nums[i];
        }

        int temp = 0;

        for (int i = 0; i < nums.Count; i++)
        {
            if (temp == sum - temp - nums[i]) return i;
            temp += nums[i];
        }

        return -1;
    }

    // 题目2
    //给定一个排序数组和一个目标值，在数组中找到目标值，并返回其索引。如果目标值不存在于数组中，返回它将会被按顺序插入的位置。
    //请必须使用时间复杂度为 O(log n) 的算法。
    int Example_2(List<int> nums, int target)
    {
        for (int i = 0; i < nums.Count; i++)
        {
            if (target <= nums[i]) return i;
        }

        return nums.Count;
    }

    // 题目3
    //以数组 intervals 表示若干个区间的集合，其中单个区间为 intervals[i] = [starti, endi] 。
    //请你合并所有重叠的区间，并返回一个不重叠的区间数组，该数组需恰好覆盖输入中的所有区间。
    int[][] Example_3(int[][] intervals)
    {
        
        return intervals;
    }
}