using System.Collections.Generic;
using System.Xml.Serialization;

namespace AshFramework.O
{
    [System.Serializable]
    public class OAssetBundleConfig
    {
        [XmlElement("oAbBases")] public List<OAbBase> oAbBases = new List<OAbBase>();
    }

    [System.Serializable]
    public class OAbBase
    {
        [XmlAttribute("Path")] public string Path { get; set; }
        [XmlAttribute("Crc")] public uint Crc { get; set; }
        [XmlAttribute("AbName")] public string AbName { get; set; }
        [XmlAttribute("AssetName")] public string AssetName { get; set; }
        [XmlElement("AbDependence")] public List<string> AbDependence { get; set; }
    }
}