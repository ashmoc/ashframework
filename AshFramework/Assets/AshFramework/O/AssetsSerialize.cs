using System.Collections.Generic;
using UnityEngine;

namespace AshFramework.O
{
    [CreateAssetMenu(fileName = "TestAssets", menuName = "AshTool/CreateAssets", order = 0)]
    public class AssetsSerialize : ScriptableObject //继承这个类就不需要再给类添加任何序列化的标签
    {
        public int Id;
        public string Name; 
        public List<int> testList = new List<int>();
    }
}