using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Xml.Serialization;
using UnityEditor;
using UnityEngine;

namespace AshFramework.O
{
    public class OBundleEditor
    {
        public static string AbConfigPath = "Assets/AshFramework/O/Editor/AssetBundleConfig.asset";

        public static string TargetAbPath =
            $"{Application.streamingAssetsPath}/AssetBundles/{ResKitUtil.GetPlatform()}".CheckAndCreateDirectory();


        public static string OAssetBundleConfigPath =
            $"{TargetAbPath}/OAssetBundleConfig.bytes";

        //Key: AB包名     Value: 路径, 所有文件夹的 Dictionary
        private static Dictionary<string, string> _allAbFolderPathDic = new Dictionary<string, string>();

        //过滤的 List
        private static List<string> _allAbFolderPaths = new List<string>();

        //所有 Prefab 的依赖
        private static Dictionary<string, List<string>> _allPrefabDependenciesDic = new Dictionary<string, List<string>>();

        ///储存所有有效路径
        private static List<string> _configFilePath = new List<string>();

        public static void OBuildAssetBundles()
        {
            _allAbFolderPaths.Clear();
            _allAbFolderPathDic.Clear();
            _allPrefabDependenciesDic.Clear();
            _configFilePath.Clear();

            AssetBundleConfig abc = AssetDatabase.LoadAssetAtPath<AssetBundleConfig>(AbConfigPath);

            foreach (var abData in abc.allAbFolderDatas)
            {
                if (_allAbFolderPathDic.ContainsKey(abData.abName))
                {
                    Debug.LogError("AB 包配置名字重复，请检查。");
                }
                else
                {
                    _allAbFolderPathDic.Add(abData.abName, abData.path);
                    _allAbFolderPaths.Add(abData.path);
                    _configFilePath.Add(abData.path);
                }
            }

            string[] allPrefabPaths = AssetDatabase.FindAssets("t:Prefab", abc.allPrefabPaths.ToArray());
            for (int i = 0; i < allPrefabPaths.Length; i++)
            {
                string path = AssetDatabase.GUIDToAssetPath(allPrefabPaths[i]);

                EditorUtility.DisplayProgressBar("查找 Prefab", $"Prefab: {path}", i * 1.0f / allPrefabPaths.Length);

                _configFilePath.Add(path);

                if (!ContainAbPath(path))
                {
                    GameObject go = AssetDatabase.LoadAssetAtPath<GameObject>(path);
                    string[] allDependencies = AssetDatabase.GetDependencies(path);
                    List<string> allDependenciesPaths = new List<string>();
                    for (int j = 0; j < allDependencies.Length; j++)
                    {
                        // Debug.Log(allDependencies[j]);
                        if (!ContainAbPath(allDependencies[j]) && !allDependencies[j].EndsWith(".cs"))
                        {
                            _allAbFolderPaths.Add(allDependencies[j]);
                            allDependenciesPaths.Add(allDependencies[j]);
                        }
                    }

                    if (_allPrefabDependenciesDic.ContainsKey(go.name))
                    {
                        Debug.LogError($"存在相同名字的 Prefab: {go.name}。");
                    }
                    else
                    {
                        _allPrefabDependenciesDic.Add(go.name, allDependenciesPaths);
                    }
                }
            }

            foreach (var name in _allAbFolderPathDic.Keys)
            {
                SetAbName(_allAbFolderPathDic[name], name);
            }

            foreach (var name in _allPrefabDependenciesDic.Keys)
            {
                SetAbName(_allPrefabDependenciesDic[name], name);
            }

            BuildAssetBundle();

            //清除设置的 Ab 包名
            // string[] oldAbNames = AssetDatabase.GetAllAssetBundleNames();
            //
            // for (int i = 0; i < oldAbNames.Length; i++)
            // {
            //     AssetDatabase.RemoveAssetBundleName(oldAbNames[i], true);
            //     EditorUtility.DisplayProgressBar("清除 Ab 包名", $"名字：{oldAbNames[i]}", i * 1.0f / oldAbNames.Length);
            // }

            EditorUtility.ClearProgressBar();
            AssetDatabase.Refresh();
        }

        public static void SetAbName(string path, string name)
        {
            AssetImporter assetImporter = AssetImporter.GetAtPath(path);
            if (assetImporter == null)
            {
                Debug.LogError($"不存在此 Ab 包路径：{path}");
            }
            else
            {
                assetImporter.assetBundleName = name;
            }
        }

        public static void SetAbName(List<string> paths, string name) => paths.ForEach(path => SetAbName(path, name));

        public static void BuildAssetBundle()
        {
            string[] allAbNames = AssetDatabase.GetAllAssetBundleNames();

            //Key: 路径       Value: 包名
            Dictionary<string, string> resPathDic = new Dictionary<string, string>();

            for (int i = 0; i < allAbNames.Length; i++)
            {
                //当前 ab 包名下的所有资源路径
                string[] allAbPaths = AssetDatabase.GetAssetPathsFromAssetBundle(allAbNames[i]);
                for (int j = 0; j < allAbPaths.Length; j++)
                {
                    if (allAbPaths[j].EndsWith(".cs")) continue;

                    Debug.Log($"Ab包：{allAbNames[i]}     包含资源路径：{allAbPaths[j]}");

                    if (ValidPath(allAbPaths[j]))
                    {
                        resPathDic.Add(allAbPaths[j], allAbNames[i]);
                    }
                }
            }

            DeleteObsAb();

            //生成自己的配置表 
            WriteData(resPathDic);

            BuildPipeline.BuildAssetBundles(TargetAbPath, BuildAssetBundleOptions.ChunkBasedCompression,
                EditorUserBuildSettings.activeBuildTarget);
        }

        static void WriteData(Dictionary<string, string> resPathDic)
        {
            OAssetBundleConfig config = new OAssetBundleConfig();
            config.oAbBases = new List<OAbBase>();

            foreach (var key in resPathDic.Keys)
            {
                OAbBase abBase = new OAbBase();
                abBase.Path = key;
                abBase.Crc = CRC32.GetCRC32(key);
                abBase.AbName = resPathDic[key];
                abBase.AssetName = key.Remove(0, key.LastIndexOf("/") + 1);
                abBase.AbDependence = new List<string>();

                string[] dependencies = AssetDatabase.GetDependencies(key);
                for (int i = 0; i < dependencies.Length; i++)
                {
                    string tempPath = dependencies[i];
                    if (tempPath == key || key.EndsWith(".cs")) continue;
                    string abName = string.Empty;
                    if (resPathDic.TryGetValue(tempPath, out abName))
                    {
                        if (abName == resPathDic[key]) continue;
                        if (!abBase.AbDependence.Contains(abName))
                        {
                            abBase.AbDependence.Add(abName);
                        }
                    }
                }

                config.oAbBases.Add(abBase);
            }

            //写入 Xml
            string xmlPath = TargetAbPath + "/OAssetBundleConfig.xml";
            if (File.Exists(xmlPath)) File.Delete(xmlPath);
            FileStream fs = new FileStream(xmlPath, FileMode.Create, FileAccess.ReadWrite, FileShare.ReadWrite);
            StreamWriter sw = new StreamWriter(fs, System.Text.Encoding.UTF8);
            XmlSerializer xs = new XmlSerializer(config.GetType());
            xs.Serialize(sw, config);
            sw.Close();
            fs.Close();

            //写入二进制
            foreach (var oAbBase in config.oAbBases)
            {
                oAbBase.Path = "";
            }

            string bytePath = TargetAbPath + "/OAssetBundleConfig.bytes";
            if (File.Exists(bytePath)) File.Delete(bytePath);
            FileStream fs2 = new FileStream(bytePath, FileMode.Create, FileAccess.ReadWrite, FileShare.ReadWrite);
            BinaryFormatter bf = new BinaryFormatter();
            bf.Serialize(fs2, config);
            fs2.Close();
        }

        /// <summary>删除没有用的 Ab 包</summary>
        static void DeleteObsAb()
        {
            string[] allAbNames = AssetDatabase.GetAllAssetBundleNames();
            DirectoryInfo directoryInfo = new DirectoryInfo(TargetAbPath);

            //获取文件夹下所有的文件
            FileInfo[] files = directoryInfo.GetFiles("*", SearchOption.AllDirectories);

            for (int i = 0; i < files.Length; i++)
            {
                if (ContainAbName(files[i].Name, allAbNames) || files[i].Name.EndsWith(".meta"))
                {
                    continue;
                }
                else
                {
                    Debug.Log($"此 Ab 包：[{files[i].Name}] 已经改名或者被删");
                    if (File.Exists(files[i].FullName))
                    {
                        File.Delete(files[i].FullName);
                    }
                }
            }
        }

        static bool ContainAbName(string name, string[] paths)
        {
            for (int i = 0; i < paths.Length; i++)
            {
                if (name == paths[i]) return true;
            }

            return false;
        }

        /// <summary>是否包含在 Ab 包里，用来做 Ab 包冗余剔除</summary>
        static bool ContainAbPath(string path)
        {
            for (int i = 0; i < _allAbFolderPaths.Count; i++)
            {
                if (path == _allAbFolderPaths[i] || (path.Contains(_allAbFolderPaths[i]) && path.Replace(_allAbFolderPaths[i], "")[0] == '/'))
                {
                    return true;
                }
            }

            return false;
        }

        /// <summary>是否有效路径</summary>
        static bool ValidPath(string path)
        {
            for (int i = 0; i < _configFilePath.Count; i++)
            {
                if (path.Contains(_configFilePath[i]))
                {
                    return true;
                }
            }

            return false;
        }
    }
}