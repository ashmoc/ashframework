using System.Collections.Generic;
using UnityEngine;

namespace AshFramework.O
{
    [CreateAssetMenu(fileName = "AssetBundleConfig", menuName = "AshTool/CreateAssetBundleConfig", order = 0)]
    public class AssetBundleConfig : ScriptableObject
    {
        //单个文件所在文件夹路径，会遍历文件夹下面所有 Prefab（名称不能重复）
        public List<string> allPrefabPaths = new List<string>();

        public List<AbData> allAbFolderDatas = new List<AbData>();

        [System.Serializable]
        public struct AbData
        {
            public string abName;
            public string path;
        }
    }
}