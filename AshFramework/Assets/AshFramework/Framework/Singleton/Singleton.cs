using System;
using System.Reflection;

namespace AshFramework
{
    public abstract class Singleton<T> where T : Singleton<T>
    {
        private static T _instance;

        public static T Instance
        {
            get
            {
                if (_instance == null)
                {
                    var constructors = typeof(T).GetConstructors(BindingFlags.Instance | BindingFlags.NonPublic);
                    var constructor = Array.Find(constructors, c => c.GetParameters().Length == 0);

                    if (constructor == null)
                    {
                        //TODO
                        throw new Exception("Non-public constructor not found!");
                    }

                    _instance = constructor.Invoke(null) as T;
                }

                return _instance;
            }
        }

        protected Singleton()
        {
        }
    }
}