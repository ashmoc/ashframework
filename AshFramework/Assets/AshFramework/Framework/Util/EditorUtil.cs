using UnityEngine;
using UnityEditor;
#if UNITY_EDITOR
public class EditorUtil : MonoBehaviour
{
    /// <summary>导出 Assets 目录下的某个文件夹为 unitypackage 包</summary>
    public static void ExportPackage(string assetsPathName, string fileName) =>
        AssetDatabase.ExportPackage($"Assets/{assetsPathName}", $"{fileName}.unitypackage",
            ExportPackageOptions.Recurse);

    /// <summary>调用菜单栏的某一个方法</summary>
    private static void ExecuteMenuItem(string menuItemPath) => EditorApplication.ExecuteMenuItem(menuItemPath);

    /// <summary>打开文件夹（路径中不要有中文）</summary>
    public static void OpenInFolder(string folderPath) => Application.OpenURL($"file:///{folderPath}");
}
#endif