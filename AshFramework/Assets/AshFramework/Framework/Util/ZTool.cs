using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public static class ZTool
{
    #region Common

    /// <summary>复制文本到剪切板</summary>
    public static void CopyText(string text) => GUIUtility.systemCopyBuffer = $"{text}";

    #endregion

    #region IO

    public static string CheckAndCreateDirectory(this string path)
    {
        if (!Directory.Exists(path))
        {
            Directory.CreateDirectory(path);
        }

        return path;
    }

    #endregion

    #region Transform

    public static void Identity(this Transform transform)
    {
        transform.localPosition = Vector3.zero;
        transform.localRotation = Quaternion.identity;
        transform.localScale = Vector3.one;
    }

    public static void SetLocalPosX(this Transform transform, float x) =>
        transform.localPosition = new Vector3(x, transform.localPosition.y, transform.localPosition.z);

    public static void SetLocalPosY(this Transform transform, float y) =>
        transform.localPosition = new Vector3(transform.localPosition.x, y, transform.localPosition.z);

    public static void SetLocalPosZ(this Transform transform, float z) =>
        transform.localPosition = new Vector3(transform.localPosition.x, transform.localPosition.y, z);

    public static void SetLocalPosXY(this Transform transform, float x, float y) =>
        transform.localPosition = new Vector3(x, y, transform.localPosition.z);

    public static void SetLocalPosXZ(this Transform transform, float x, float z) =>
        transform.localPosition = new Vector3(x, transform.localPosition.y, z);

    public static void SetLocalPosYZ(this Transform transform, float y, float z) =>
        transform.localPosition = new Vector3(transform.localPosition.x, y, z);

    public static void AddChild(this Transform transform, Transform child) => child.SetParent(transform);

    #endregion

    #region Math

    public static bool IsInRange(int range, int minInclusive = 0, int maxExclusive = 100) =>
        Random.Range(minInclusive, maxExclusive) < range;

    public static T GetRandomFrom<T>(params T[] values) => values[Random.Range(0, values.Length)];

    #endregion
}