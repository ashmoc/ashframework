using System;
using System.Collections.Generic;

namespace AshFramework
{
    public class MassageDispatcher
    {
        private static Dictionary<string, Action<object>> _registMassages = new Dictionary<string, Action<object>>();

        public static void Register(string name, Action<object> action)
        {
            if (!_registMassages.ContainsKey(name))
            {
                _registMassages.Add(name, _ => { });
            }

            _registMassages[name] += action;
        }

        public static void UnRegisterAll(string name)
        {
            if (!_registMassages.ContainsKey(name)) return;
            _registMassages.Remove(name);
        }

        public static void UnRegister(string name, Action<object> action)
        {
            if (_registMassages.ContainsKey(name))
            {
                _registMassages[name] -= action;
            }
        }

        public static void Send(string name, object data)
        {
            if (_registMassages.ContainsKey(name))
            {
                _registMassages[name](data);
            }
        }
    }

    public class MassageRecord
    {
        private static Stack<MassageRecord> _massageRecordPool = new Stack<MassageRecord>();

        public static MassageRecord Allocate(string name, Action<object> action)
        {
            if (_massageRecordPool.Count <= 0) return new MassageRecord(name, action);

            var massageRecord = _massageRecordPool.Pop();
            massageRecord.Name = name;
            massageRecord.Action = action;
            return massageRecord;
        }

        public string Name { get; private set; }
        public Action<object> Action { get; private set; }

        private MassageRecord(string name, Action<object> action)
        {
            Name = name;
            Action = action;
        }

        public void Recycle()
        {
            Name = null;
            Action = null;

            _massageRecordPool.Push(this);
        }
    }
}