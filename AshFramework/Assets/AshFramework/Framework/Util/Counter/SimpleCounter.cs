using UnityEngine;

namespace AshFramework
{
    interface ISimpleCounter
    {
        int Count { get; }

        void Retain(object owner = null);

        void Release(object owner = null);
    }

    public class SimpleCounter : ISimpleCounter
    {
        public int Count { get; private set; }

        public void Retain(object owner = null)
        {
            Count++;
        }

        public void Release(object owner = null)
        {
            if (Count == 0) return;

            Count--;

            if (Count == 0)
            {
                OnZeroCount();
            }
        }

        protected virtual void OnZeroCount()
        {
        }
    }
}