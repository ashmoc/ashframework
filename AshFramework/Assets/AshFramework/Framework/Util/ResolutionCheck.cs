using System.Collections;
using System.Collections.Generic;
using UnityEngine;

#if UNITY_EDITOR
using UnityEditor;
#endif

namespace AshFramework
{
    public class ResolutionCheck
    {
        public static bool IsPadResolution() => InAspectRange(4.0f / 3);
        public static bool IsPhoneResolution() => InAspectRange(16.0f / 9);
        public static bool IsiPhone15Resolution() => InAspectRange(3.0f / 2);
        public static bool IsiPhoneXResolution() => InAspectRange(2436.0f / 1125);

        public static float GetAspectRatio()
        {
            bool isLandscape = Screen.width > Screen.height;
            return isLandscape ? (float)Screen.width / Screen.height : (float)Screen.height / Screen.width;
        }

        public static bool InAspectRange(float targetAspectRatio)
        {
            var aspect = GetAspectRatio();
            return aspect > targetAspectRatio - 0.05 && aspect < targetAspectRatio + 0.05;
        }
    }
}