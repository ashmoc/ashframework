﻿using System;
using System.Collections;
using UnityEngine;

namespace AshFramework
{
    public class TimerUtil : MonoBehaviour
    {
        public void Delay(float seconds, Action onFinished)
        {
            StartCoroutine(DelayCoroutine(seconds, onFinished));
        }

        private static IEnumerator DelayCoroutine(float seconds, Action onFinished)
        {
            yield return new WaitForSeconds(seconds);
            onFinished();
        }
    }
}