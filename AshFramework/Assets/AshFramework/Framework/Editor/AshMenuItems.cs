using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using AshFramework.O;
using UnityEngine;
using UnityEditor;

//谋定而后动 知止而有得
namespace AshFramework
{
    public class AshMenuItems
    {
        [MenuItem("Ash/示例", false, 9999)]
        public static void Clicked()
        {
            EditorApplication.isPlaying = true;
            new GameObject("Example").AddComponent<Example>();
        }

        [MenuItem("Ash/屏幕宽高比判断", false, 0)]
        private static void ResolutionCheckMenuItem()
        {
            Debug.Log(ResolutionCheck.IsPhoneResolution() ? "是 Phone" : "不是 Phone");
            Debug.Log(ResolutionCheck.IsPadResolution() ? "是 Pad" : "不是 Pad");
            Debug.Log(ResolutionCheck.IsiPhone15Resolution() ? "是 iPhone4s" : "不是 iPhone4s");
            Debug.Log(ResolutionCheck.IsiPhoneXResolution() ? "是 iPhoneX" : "不是 iPhoneX");
        }

        [MenuItem("Ash/Build AssetBundles", false, 1)]
        static void BuildAssetBundles() => AssetBundleExporter.BuildAssetBundles();

        [MenuItem("Ash/Simulation Mode", false, 2)]
        private static void ChangeSimulationMode() => SimulationModeMenu.IsSimulationMode = !SimulationModeMenu.IsSimulationMode;

        [MenuItem("Ash/导出 Ash %E", false, 999)] //% 表示 Ctrl
        private static void ExportAshPackageMenuItem() => Exporter.ExportAshPackageMenuItem();

        [MenuItem("Ash/输出所有 AssetBundle 名字", false)]
        private static void GetAllAssetBundleName()
        {
            var resData = ResData.Instance;

            var names = AssetDatabase.GetAllAssetBundleNames();
            foreach (var name in names)
            {
                var assetBundleData = new AssetBundleData()
                {
                    Name = name,
                    DependencyBundleNames = AssetDatabase.GetAssetBundleDependencies(name, false)
                };

                var paths = AssetDatabase.GetAssetPathsFromAssetBundle(name);
                foreach (var path in paths)
                {
                    var assetData = new AssetData()
                    {
                        Name = path.Split('/').Last().Split('.').First(),
                        OwnerBundleName = name
                    };

                    assetBundleData.AssetDatas.Add(assetData);

                    // var go = AssetDatabase.LoadAssetAtPath<Object>(path);
                    // Debug.Log("TYPE: " + go.GetType());
                }

                resData.AssetBundleDatas.Add(assetBundleData);
            }

            resData.AssetBundleDatas.ForEach(a =>
            {
                a.AssetDatas.ForEach(b => { Debug.Log($"AssetBundleData: {a.Name}   AssetData: {b.Name}"); });

                foreach (var name in a.DependencyBundleNames)
                {
                    Debug.Log($"AssetBundleData: {a.Name}   Dependency: {name}");
                }
            });
        }

        #region Toggle

        [MenuItem("Ash/Simulation Mode", true)]
        public static bool ToggleSimulationModeValidate()
        {
            Menu.SetChecked("Ash/Simulation Mode", SimulationModeMenu.IsSimulationMode);
            return true;
        }

        #endregion

        #region OF

        public static string ABCONFIGPATH = "Assets/AshFramework/OF/Editor/AssetBundleConfig.asset";

        [MenuItem("Ash/OF/Build AssetBundles", false, 1)]
        static void OFBuildAssetBundles() => OBundleEditor.OBuildAssetBundles();

        [MenuItem("Ash/OF/Test", false, 999)]
        static void OFTest()
        {
            // TextAsset ta = AssetDatabase.LoadAssetAtPath<TextAsset>(OBundleEditor.OAssetBundleConfigPath);
            TextAsset ta = AssetDatabase.LoadAssetAtPath<TextAsset>("Assets/OAssetBundleConfig.bytes");
            MemoryStream stream = new MemoryStream(ta.bytes);
            BinaryFormatter bf = new BinaryFormatter();
            OAssetBundleConfig abConfig = (OAssetBundleConfig)bf.Deserialize(stream);
            stream.Close();
            string path = "Assets/GameData/Prefabs/Attack.prefab";
            uint crc = CRC32.GetCRC32(path);
            OAbBase abBase = null;
            for (int i = 0; i < abConfig.oAbBases.Count; i++)
            {
                if (abConfig.oAbBases[i].Crc == crc)
                {
                    abBase = abConfig.oAbBases[i];
                }
            }

            for (int i = 0; i < abBase.AbDependence.Count; i++)
            {
                AssetBundle.LoadFromFile($"{OBundleEditor.TargetAbPath}/{abBase.AbDependence[i]}");
            }

            AssetBundle assetBundle = AssetBundle.LoadFromFile($"{OBundleEditor.TargetAbPath}/{abBase.AbName}");
            GameObject go = GameObject.Instantiate(assetBundle.LoadAsset<GameObject>(abBase.AssetName));
        }

        #endregion
    }
}