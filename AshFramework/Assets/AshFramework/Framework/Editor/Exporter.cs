using System;
using System.IO;
using UnityEngine;
using UnityEditor;

namespace AshFramework
{
    public class Exporter
    {
        public static void ExportAshPackageMenuItem()
        {
            EditorUtil.ExportPackage("AshFramework", $"AshFramework_{DateTime.Now.ToString("yyyyMMdd_HH")}");
            EditorUtil.OpenInFolder(Path.Combine(Application.dataPath, "../"));
        }
    }
}