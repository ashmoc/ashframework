using System;
using UnityEngine;
using Object = UnityEngine.Object;

namespace AshFramework
{
    public class ResourceRes : Res
    {
        private string _assetPath;

        public ResourceRes(string assetPath)
        {
            _assetPath = assetPath.Substring("resources://".Length);
            Name = assetPath;
            State = ResState.Waiting;
        }

        public override bool LoadSync()
        {
            State = ResState.Loading;
            Asset = Resources.Load(_assetPath);
            State = ResState.Loaded;

            return Asset;
        }

        public override void LoadAsync()
        {
            State = ResState.Loading;

            var request = Resources.LoadAsync(_assetPath);

            request.completed += operation =>
            {
                Asset = request.asset;
                State = ResState.Loaded;
            };
        }

        private protected override void OnReleaseRes()
        {
            if (Asset is GameObject)
            {
                Resources.UnloadUnusedAssets();
            }
            else
            {
                Resources.UnloadAsset(Asset);
            }

            ResManager.Instance.AllLoadedReses.Remove(this);
            Asset = null;
        }
    }
}