using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Object = UnityEngine.Object;

namespace AshFramework
{
    public enum ResState
    {
        Waiting,
        Loading,
        Loaded
    }

    public abstract class Res : SimpleCounter
    {
        private ResState _state;
        
        public ResState State
        {
            get => _state;
            protected set
            {
                _state = value;
                if (_state == ResState.Loaded)
                {
                    if (OnLoadedEvent != null)
                    {
                        OnLoadedEvent.Invoke(this);
                    }
                }
            }
        }

        public Object Asset { get; protected set; }

        public string Name { get; protected set; }
        private string _assetPath;

        public abstract bool LoadSync();
        public abstract void LoadAsync();

        private protected abstract void OnReleaseRes();

        protected override void OnZeroCount()
        {
            OnReleaseRes();
        }

        private event Action<Res> OnLoadedEvent;

        public void RegisterOnLoadedEvent(Action<Res> onLoaded)
        {
            OnLoadedEvent += onLoaded;
        }

        public void UnRegisterOnLoadedEvent(Action<Res> onLoaded)
        {
            OnLoadedEvent -= onLoaded;
        }
    }
}