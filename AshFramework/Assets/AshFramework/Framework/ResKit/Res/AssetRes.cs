using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace AshFramework
{
    public class AssetRes : Res
    {
        private string _ownerBundlePath;

        private ResLoader _resLoader = new ResLoader();

        public AssetRes(string assetName, string ownerBundlePath)
        {
            Name = assetName;
            _ownerBundlePath = $"{ResKitUtil.GetAssetBundlePath(assetName)}/{ownerBundlePath}";
            State = ResState.Waiting;
        }

        public override bool LoadSync()
        {
            State = ResState.Loading;

            var ownerBundle = _resLoader.LoadSync<AssetBundle>(_ownerBundlePath);

            if (ResManager.IsSimulationMode)
            {
#if UNITY_EDITOR
                var assetPaths = UnityEditor.AssetDatabase.GetAssetPathsFromAssetBundleAndAssetName(_ownerBundlePath, Name);
                Asset = UnityEditor.AssetDatabase.LoadAssetAtPath<Object>(assetPaths[0]);
#endif
            }
            else
            {
                Asset = ownerBundle.LoadAsset(Name);
            }

            State = ResState.Loaded;

            return Asset;
        }

        public override void LoadAsync()
        {
            State = ResState.Loading;
            _resLoader.LoadAsync<AssetBundle>(_ownerBundlePath, ownerBundle =>
            {
                if (ResManager.IsSimulationMode)
                {
#if UNITY_EDITOR
                    var assetPaths = UnityEditor.AssetDatabase.GetAssetPathsFromAssetBundleAndAssetName(_ownerBundlePath, Name);
                    Asset = UnityEditor.AssetDatabase.LoadAssetAtPath<Object>(assetPaths[0]);
#endif
                }
                else
                {
                    var assetBundleRequest = ownerBundle.LoadAssetAsync(Name);
                    assetBundleRequest.completed += operation =>
                    {
                        Asset = assetBundleRequest.asset;
                        State = ResState.Loaded;
                    };
                }
            });
        }

        private protected override void OnReleaseRes()
        {
            if (Asset is GameObject)
            {
            }
            else
            {
                Resources.UnloadAsset(Asset);
            }

            Asset = null;
            _resLoader.ReleaseAll();
            ResManager.Instance.AllLoadedReses.Remove(this);
            _resLoader = null;
        }
    }
}