using System;
using UnityEngine;
using Object = UnityEngine.Object;

namespace AshFramework
{
    public class AssetBundleRes : Res
    {
        public AssetBundle AssetBundle
        {
            get => Asset as AssetBundle;
            set => Asset = value;
        }

        private string _path;

        public AssetBundleRes(string assetName)
        {
            _path = ResKitUtil.GetAssetBundlePath(assetName);
            Name = assetName;
            State = ResState.Waiting;
        }

        private ResLoader _resLoader = new ResLoader();

        public override bool LoadSync()
        {
            State = ResState.Loading;

            var dependencyBundleNames = ResData.Instance.GetDirectDependencies(Name);

            foreach (var name in dependencyBundleNames)
            {
                _resLoader.LoadSync<AssetBundle>(name);
            }

            if (ResManager.IsSimulationMode)
            {
                AssetBundle = AssetBundle.LoadFromFile(_path);
            }

            State = ResState.Loaded;

            return AssetBundle;
        }

        public override void LoadAsync()
        {
            State = ResState.Loading;

            LoadDependencyBundlesAsync(() =>
            {
                if (ResManager.IsSimulationMode)
                {
                    State = ResState.Loaded;
                }
                else
                {
                    var request = AssetBundle.LoadFromFileAsync(_path);

                    request.completed += operation =>
                    {
                        AssetBundle = request.assetBundle;
                        State = ResState.Loaded;
                    };
                }
            });
        }

        private void LoadDependencyBundlesAsync(Action onAllLoaded)
        {
            var dependencyBundleNames = ResData.Instance.GetDirectDependencies(Name);
            int loadedCount = 0;

            if (dependencyBundleNames.Length == 0)
            {
                onAllLoaded();
            }
            else
            {
                foreach (var name in dependencyBundleNames)
                {
                    _resLoader.LoadAsync<AssetBundle>(name, dependBundle =>
                    {
                        loadedCount++;
                        if (loadedCount == dependencyBundleNames.Length)
                        {
                            onAllLoaded();
                        }
                    });
                }
            }
        }

        private protected override void OnReleaseRes()
        {
            if (AssetBundle != null)
            {
                AssetBundle.Unload(true);
                AssetBundle = null;
                _resLoader.ReleaseAll();
                _resLoader = null;
            }

            ResManager.Instance.AllLoadedReses.Remove(this);
        }
    }
}