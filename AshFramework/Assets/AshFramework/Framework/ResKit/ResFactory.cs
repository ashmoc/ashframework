using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace AshFramework
{
    public class ResFactory : MonoBehaviour
    {
        public static Res Create(string assetName, string assetBundleName)
        {
            Res res = null;

            if (assetBundleName != null)
            {
                res = new AssetRes(assetName, assetBundleName);
            }
            else if (assetName.StartsWith("resources://"))
            {
                res = new ResourceRes(assetName);
            }
            else
            {
                res = new AssetBundleRes(assetName);
            }

            return res;
        }
    }
}