using System;
using System.Collections.Generic;
using Object = UnityEngine.Object;

namespace AshFramework
{
    public class ResLoader
    {
        private List<Res> _resRecords = new List<Res>();

        public T LoadSync<T>(string assetName) where T : Object => DoLoadSync<T>(assetName);

        public T LoadSyncFromAssetBundle<T>(string assetBundleName, string assetName) where T : Object =>
            DoLoadSync<T>(assetName, assetBundleName);

        public void LoadAsync<T>(string assetName, Action<T> onLoaded) where T : Object => DoLoadAsync(assetName, null, onLoaded);

        public void LoadAsync<T>(string ownerBundlePath, string assetName, Action<T> onLoaded) where T : Object =>
            DoLoadAsync(assetName, ownerBundlePath, onLoaded);

        private T DoLoadSync<T>(string assetName, string assetBundleName = null) where T : Object
        {
            var res = CheckResExist(assetName);

            if (res != null)
            {
                switch (res.State)
                {
                    case ResState.Loading:
                        throw new Exception($"请不要在异步加载资源 [{res.Name}] 时进行同步加载。");
                    case ResState.Loaded:
                        return res.Asset as T;
                }
            }

            res = CreateRes(assetName, assetBundleName);
            res.LoadSync();

            return res.Asset as T;
        }

        private void DoLoadAsync<T>(string assetName, string ownerBundlePath, Action<T> onLoaded) where T : Object
        {
            var res = CheckResExist(assetName);

            Action<Res> onResLoaded = null;

            onResLoaded = loadedRes =>
            {
                onLoaded(loadedRes.Asset as T);
                res.UnRegisterOnLoadedEvent(onResLoaded);
            };

            if (res != null)
            {
                switch (res.State)
                {
                    case ResState.Loading:
                        res.RegisterOnLoadedEvent(onResLoaded);
                        break;
                    case ResState.Loaded:
                        onLoaded(res.Asset as T);
                        break;
                }

                return;
            }

            res = CreateRes(assetName, ownerBundlePath);
            res.RegisterOnLoadedEvent(onResLoaded);
            res.LoadAsync();
        }

        public void ReleaseAll()
        {
            _resRecords.ForEach(res => res.Release());
            _resRecords.Clear();
        }

        /// <summary>检查资源是否已经存在</summary>
        private Res CheckResExist(string assetName)
        {
            var res = GetResFromRecords(assetName);
            if (res != null)
            {
                return res;
            }

            res = GetFromResManager(assetName);
            if (res != null)
            {
                AddRes2Record(res);
                return res;
            }

            return res;
        }

        private Res CreateRes(string assetName, string assetBundleName = null)
        {
            Res res = ResFactory.Create(assetName, assetBundleName);
            ResManager.Instance.AllLoadedReses.Add(res);
            AddRes2Record(res);
            return res;
        }

        private Res GetResFromRecords(string assetName)
        {
            return _resRecords.Find(res => res.Name == assetName);
        }

        private Res GetFromResManager(string assetName)
        {
            return ResManager.Instance.AllLoadedReses.Find(res => res.Name == assetName);
        }

        private void AddRes2Record(Res res)
        {
            _resRecords.Add(res);
            res.Retain();
        }
    }
}