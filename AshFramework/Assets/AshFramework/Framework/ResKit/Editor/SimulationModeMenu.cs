using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

namespace AshFramework
{
    public class SimulationModeMenu
    {
        public static bool IsSimulationMode
        {
            get => ResManager.IsSimulationMode;
            set => ResManager.IsSimulationMode = value;
        }
    }
}