using System.IO;
using System.Linq;
using UnityEditor;
using UnityEngine;

namespace AshFramework
{
    public class AssetBundleExporter : MonoBehaviour
    {
        public static void BuildAssetBundles()
        {
            string path = $"{Application.streamingAssetsPath}/AssetBundles/{ResKitUtil.GetPlatform()}".CheckAndCreateDirectory();

            BuildPipeline.BuildAssetBundles(path, BuildAssetBundleOptions.ChunkBasedCompression, EditorUserBuildSettings.activeBuildTarget);

            var versionConfigFilePath = $"{Application.streamingAssetsPath}/ResVersion.json";

            var resVersion = new ResVersion()
            {
                version = 5,
                assetBundleNames = AssetDatabase.GetAllAssetBundleNames().ToList()
            };

            var resVersionJson = JsonUtility.ToJson(resVersion, true); //true 让 json 文件有缩进
            File.WriteAllText(versionConfigFilePath, resVersionJson);

            AssetDatabase.Refresh();
        }
    }
}