using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;

namespace AshFramework
{
    [Serializable]
    public class ResVersion
    {
        public int version;

        public List<string> assetBundleNames = new List<string>();
    }

    public class FakeResServer : MonoSingleton<FakeResServer>
    {
        public static string TempAssetBundlesPath => $"{Application.persistentDataPath}/TempAssetBundles/";

        public void GetRemoteResVersion(Action<int> downloadDone)
        {
            StartCoroutine(HotUpdateManager.instance.Config.RequestRemoteResVersion(resVersion => { downloadDone(resVersion.version); }));
        }

        public void DownloadRes(Action<ResVersion> onResDownloaded)
        {
            StartCoroutine(HotUpdateManager.instance.Config.RequestRemoteResVersion(onResDownloaded));
        }
    }
}