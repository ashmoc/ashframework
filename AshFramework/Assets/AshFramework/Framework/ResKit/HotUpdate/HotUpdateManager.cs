using System;
using System.IO;
using UnityEngine;

namespace AshFramework
{
    public enum HotUpdateState
    {
        /// <summary>从未更新过</summary>
        NeverUpdate,

        /// <summary>更新过</summary>
        Updated,

        /// <summary>覆盖安装，但是有旧的资源</summary>
        Overrided
    }

    public class HotUpdateManager : MonoSingleton<HotUpdateManager>
    {
        public HotUpdateState state;

        public HotUpdateConfig Config { get; set; }

        private void Awake()
        {
            Config = new HotUpdateConfig();
        }

        public void CheckState(Action done)
        {
            var persistResVersion = Config.LoadHotUpdateAssetBundlesFolderResVersion();

            if (persistResVersion == null)
            {
                state = HotUpdateState.NeverUpdate;
                done();
            }
            else
            {
                StartCoroutine(Config.GetStreamingAssetResVersion(resVersion =>
                {
                    if (persistResVersion.version > resVersion.version)
                    {
                        state = HotUpdateState.Updated;
                    }
                    else
                    {
                        state = HotUpdateState.Overrided;
                    }

                    done();
                }));
            }
        }

        public void GetLocalResVersion(Action<int> getLocalResVersion)
        {
            if (state == HotUpdateState.NeverUpdate || state == HotUpdateState.Overrided)
            {
                StartCoroutine(Config.GetStreamingAssetResVersion(resVersion => getLocalResVersion(resVersion.version)));
                return;
            }

            getLocalResVersion(Config.LoadHotUpdateAssetBundlesFolderResVersion().version);
        }

        public void HasNewVersionRes(Action<bool> onResult)
        {
            FakeResServer.Instance.GetRemoteResVersion(removeVersion =>
            {
                GetLocalResVersion(localVersion =>
                {
                    var result = removeVersion > localVersion;
                    onResult(result);
                });
            });
        }

        public void UpdateRes(Action onUpdateDone)
        {
            Debug.Log("开始更新");
            Debug.Log("下载资源...");
            FakeResServer.Instance.DownloadRes(resVersion =>
            {
                RepalceRes(resVersion);
                Debug.Log("结束更新");
                onUpdateDone();
            });
        }

        private void DownloadRes()
        {
        }

        private void RepalceRes(ResVersion remoteResVersion)
        {
            var tempAssetBundleFolders = FakeResServer.TempAssetBundlesPath;
            var assetBundleFolders = Config.HotUpdateAssetBundleFolder;

            if (Directory.Exists(assetBundleFolders))
            {
                Directory.Delete(assetBundleFolders, true);
            }

            Directory.Move(tempAssetBundleFolders, assetBundleFolders);

            if (Directory.Exists(tempAssetBundleFolders))
            {
                Directory.Delete(tempAssetBundleFolders, true);
            }
        }
    }
}