using System;
using System.Collections;
using System.IO;
using UnityEngine;

namespace AshFramework
{
    public class HotUpdateConfig
    {
        public virtual string HotUpdateAssetBundleFolder => $"{Application.persistentDataPath}/AssetBundles/";

        public virtual string LocalAssetBundlesFolder =>
            $"{Application.streamingAssetsPath}/AssetBundles/{ResKitUtil.GetPlatform()}/";

        public virtual string LocalResVersionFilePath =>
            $"{Application.streamingAssetsPath}/AssetBundles/{ResKitUtil.GetPlatform()}/ResVersion.json";

        public virtual string RemoteResVersionURL =>
            $"{Application.dataPath}/AshFramework/Framework/ResKit/HotUpdate/Remote/RemoteResVersion.json";

        public virtual string RemoteAssetBundlesURLBase => $"{Application.dataPath}/AshFramework/Framework/ResKti/HotUpdate/Remote/";

        public virtual ResVersion LoadHotUpdateAssetBundlesFolderResVersion()
        {
            var jsonPath = $"{HotUpdateAssetBundleFolder}/ResVersion.json";
            if (!File.Exists(jsonPath)) return null;

            var json = File.ReadAllText(jsonPath);
            var resVersion = JsonUtility.FromJson<ResVersion>(json);
            return resVersion;
        }

        public virtual IEnumerator GetStreamingAssetResVersion(Action<ResVersion> action)
        {
            string path = LocalResVersionFilePath;
            var www = new WWW(path);
            yield return www;
            var resVersion = JsonUtility.FromJson<ResVersion>(www.text);
            action(resVersion);
        }

        public virtual IEnumerator RequestRemoteResVersion(Action<ResVersion> onResDownloaded)
        {
            var path = RemoteResVersionURL;
            var www = new WWW(path);
            yield return www;
            var jsonString = www.text;
            var resVersion = JsonUtility.FromJson<ResVersion>(jsonString);
            onResDownloaded(resVersion);
        }
    }
}