using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace AshFramework
{
    public class ResManager : MonoSingleton<ResManager>
    {
        private const string SimulationModeKey = "SimulationModeKey";

        public static bool IsSimulationMode
        {
            get
            {
#if UNITY_EDITOR
                return UnityEditor.EditorPrefs.GetBool(SimulationModeKey, true);
#endif
                return false;
            }
            set
            {
#if UNITY_EDITOR
                UnityEditor.EditorPrefs.SetBool(SimulationModeKey, value);
#endif
            }
        }

        public List<Res> AllLoadedReses = new List<Res>();

#if UNITY_EDITOR

        private void OnGUI()
        {
            if (Input.GetKey(KeyCode.F12))
            {
                GUILayout.BeginVertical("box");

                AllLoadedReses.ForEach((res) => GUILayout.Label($"Name:[{res.Name}] Count:[{res.Count}] State:[{res.State}]"));

                GUILayout.EndVertical();
            }
        }
#endif
    }
}