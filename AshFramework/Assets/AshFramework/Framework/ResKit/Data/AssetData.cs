using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace AshFramework
{
    public class AssetData
    {
        public string Name;

        public string OwnerBundleName;
    }
}