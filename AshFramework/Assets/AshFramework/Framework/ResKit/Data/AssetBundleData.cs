using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace AshFramework
{
    public class AssetBundleData
    {
        public string Name;
        private List<ResData> _resDatas = new List<ResData>();
        public List<AssetData> AssetDatas = new List<AssetData>();

        public string[] DependencyBundleNames;
    }
}