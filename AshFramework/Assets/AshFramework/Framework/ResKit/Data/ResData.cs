using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEngine;

namespace AshFramework
{
    public class ResData : Singleton<ResData>
    {
        private static AssetBundleManifest _manifest;

        public List<AssetBundleData> AssetBundleDatas = new List<AssetBundleData>();

        private ResData()
        {
            Load();
        }

        private void Load()
        {
            if (ResManager.IsSimulationMode)
            {
#if UNITY_EDITOR
                var names = AssetDatabase.GetAllAssetBundleNames();
                foreach (var name in names)
                {
                    var assetBundleData = new AssetBundleData()
                    {
                        Name = name,
                        DependencyBundleNames = AssetDatabase.GetAssetBundleDependencies(name, false)
                    };

                    var paths = AssetDatabase.GetAssetPathsFromAssetBundle(name);
                    foreach (var path in paths)
                    {
                        var assetData = new AssetData()
                        {
                            Name = path.Split('/').Last().Split('.').First(),
                            OwnerBundleName = name
                        };

                        assetBundleData.AssetDatas.Add(assetData);
                    }

                    AssetBundleDatas.Add(assetBundleData);
                }

                AssetBundleDatas.ForEach(a =>
                {
                    a.AssetDatas.ForEach(b => { Debug.Log($"AssetBundleData: {a.Name}   AssetData: {b.Name}"); });

                    foreach (var name in a.DependencyBundleNames)
                    {
                        Debug.Log($"AssetBundleData: {a.Name}   Dependency: {name}");
                    }
                });
#endif
            }
            else
            {
                var mainBundle = AssetBundle.LoadFromFile(ResKitUtil.GetAssetBundlePath(ResKitUtil.GetPlatform()));
                _manifest = mainBundle.LoadAsset<AssetBundleManifest>("AssetBundleManifest");
            }
        }

        public string[] GetDirectDependencies(string bundleName)
        {
            if (ResManager.IsSimulationMode)
            {
                return AssetBundleDatas.Find(o => o.Name == bundleName).DependencyBundleNames;
            }

            return _manifest.GetDirectDependencies(bundleName);
        }
    }
}