using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace AshFramework
{
    public class ResKitUtil
    {
        public static string GetAssetBundlePath(string assetBundleName)
        {
            if (HotUpdateManager.Instance.state == HotUpdateState.NeverUpdate ||
                HotUpdateManager.Instance.state == HotUpdateState.Overrided)
            {
                return HotUpdateManager.Instance.Config.LocalAssetBundlesFolder + assetBundleName;
            }

            return HotUpdateManager.Instance.Config.HotUpdateAssetBundleFolder + assetBundleName;
        }

        public static string GetPlatform()
        {
#if UNITY_EDITOR
            return GetPlatform(UnityEditor.EditorUserBuildSettings.activeBuildTarget);
#else
            return GetPlatform(Application.platform);
#endif
        }

        private static string GetPlatform(RuntimePlatform runtimePlatform)
        {
            switch (runtimePlatform)
            {
                case RuntimePlatform.OSXPlayer:
                    return "OSX";
                case RuntimePlatform.WindowsPlayer:
                    return "Windows";
                case RuntimePlatform.IPhonePlayer:
                    return "iOS";
                case RuntimePlatform.Android:
                    return "Android";
                case RuntimePlatform.LinuxPlayer:
                    return "Linux";
                case RuntimePlatform.WebGLPlayer:
                    return "WebGL";
                default:
                    return string.Empty;
            }
        }

        #region UnityEditor

#if UNITY_EDITOR

        private static string GetPlatform(UnityEditor.BuildTarget buildTarget)
        {
            switch (buildTarget)
            {
                case UnityEditor.BuildTarget.StandaloneOSX:
                    return "OSX";

                case UnityEditor.BuildTarget.StandaloneWindows:
                case UnityEditor.BuildTarget.StandaloneWindows64:
                    return "Windows";

                case UnityEditor.BuildTarget.iOS:
                    return "iOS";

                case UnityEditor.BuildTarget.Android:
                    return "Android";

                case UnityEditor.BuildTarget.StandaloneLinux64:
                    return "Linux";

                case UnityEditor.BuildTarget.WebGL:
                    return "WebGL";

                default:
                    return string.Empty;
            }
        }

#endif

        #endregion
    }
}