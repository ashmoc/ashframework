using System;
using System.Collections;
using System.Collections.Generic;
using AshFramework;
using UnityEngine;

public abstract class AshBehavior : MonoBehaviour
{
    #region MassageDispatcher

    private List<MassageRecord> _massageRecords = new List<MassageRecord>();

    public void SendMassage(string massageName, object data)
    {
        MassageDispatcher.Send(massageName, data);
    }

    public void RegistMassage(string massageName, Action<object> action)
    {
        MassageDispatcher.Register(massageName, action);
        _massageRecords.Add(MassageRecord.Allocate(massageName, action));
    }

    public void UnRegistMassage(string massageName)
    {
        var selectedRecords = _massageRecords.FindAll(record => record.Name == massageName);

        selectedRecords.ForEach(record =>
        {
            MassageDispatcher.UnRegister(record.Name, record.Action);
            record.Recycle();
            _massageRecords.Remove(record);
        });

        selectedRecords.Clear();
    }

    public void UnRegistMassage(string massageName, Action<object> action)
    {
        var selectedRecords = _massageRecords.FindAll(record => record.Name == massageName && record.Action == action);
        selectedRecords.ForEach(record =>
        {
            MassageDispatcher.UnRegister(record.Name, record.Action);
            record.Recycle();
            _massageRecords.Remove(record);
        });

        selectedRecords.Clear();
    }

    #endregion

    private void OnDestroy()
    {
        OnBeforeDestroy();

        foreach (var record in _massageRecords)
        {
            MassageDispatcher.UnRegister(record.Name, record.Action);
            record.Recycle();
        }

        _massageRecords.Clear();
    }

    protected abstract void OnBeforeDestroy();

    public void Delay(float seconds, Action onFinished)
    {
        StartCoroutine(DelayCoroutine(seconds, onFinished));
    }

    private IEnumerator DelayCoroutine(float seconds, Action onFinished)
    {
        yield return new WaitForSeconds(seconds);
        onFinished();
    }
}