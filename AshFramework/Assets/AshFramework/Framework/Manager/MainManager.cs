using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace AshFramework
{
    public enum EnviourmentMode
    {
        Developing,
        Test,
        Production
    }

    public abstract class MainManager : MonoBehaviour
    {
        public EnviourmentMode Mode;

        private static EnviourmentMode _sharedMode;
        private static bool _isModeSetted = false;

        void Start()
        {
            if (!_isModeSetted)
            {
                _sharedMode = Mode;
                _isModeSetted = true;
            }

            switch (_sharedMode)
            {
                case EnviourmentMode.Developing:
                    LaunchInDevelopingMode();
                    break;
                case EnviourmentMode.Test:
                    LaunchInTestMode();
                    break;
                case EnviourmentMode.Production:
                    LaunchInProductionMode();
                    break;
            }
        }

        protected abstract void LaunchInDevelopingMode();
        protected abstract void LaunchInTestMode();
        protected abstract void LaunchInProductionMode();
    }
}