using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace AshFramework
{
    public enum UILayer
    {
        Background,
        Common,
        Top
    }

    public class GUIManager
    {
        private static GameObject _uiRoot;

        public static GameObject UIRoot
        {
            get
            {
                if (_uiRoot == null)
                {
                    _uiRoot = Object.Instantiate(Resources.Load<GameObject>("UIRoot"));
                    _uiRoot.name = "UIRoot";
                }

                return _uiRoot;
            }
        }

        private static Dictionary<string, GameObject> _panelDic = new Dictionary<string, GameObject>();

        public static void SetResolution(float width, float height, float matchWidthOrHeight)
        {
            var canvasScaler = UIRoot.GetComponent<CanvasScaler>();
            canvasScaler.referenceResolution = new Vector2(width, height);
            canvasScaler.matchWidthOrHeight = matchWidthOrHeight;
        }

        public static GameObject LoadPanel(string name, UILayer uiLayer)
        {
            var prefab = Resources.Load<GameObject>(name);
            var parent = GameObject.Find($"{uiLayer}").transform;
            GameObject go = Object.Instantiate(prefab, parent);
            go.name = name;

            _panelDic.Add(name, go);

            var prefabRect = go.transform as RectTransform;
            if (prefabRect != null)
            {
                prefabRect.offsetMin = Vector2.zero;
                prefabRect.offsetMax = Vector2.zero;
                prefabRect.anchoredPosition3D = Vector3.zero;
                prefabRect.anchorMin = Vector2.zero;
                prefabRect.anchorMax = Vector2.one;
            }

            return go;
        }

        public static void UnloadPanel(string name)
        {
            if (_panelDic.ContainsKey(name))
            {
                Object.Destroy(_panelDic[name]);
            }
        }
    }
}