using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace AshFramework
{
    public class LevelManager
    {
        private static List<string> _levelNames;

        public static int Index { get; set; }

        public static void Init(List<string> levelNames)
        {
            _levelNames = levelNames;

            Index = 0;
        }

        public static void LoadCurrent()
        {
            SceneManager.LoadScene(_levelNames[Index]);
        }

        public static void LoadNext()
        {
            Index = Index + 1 >= _levelNames.Count ? 0 : Index + 1;
            SceneManager.LoadScene(_levelNames[Index]);
        }
    }
}