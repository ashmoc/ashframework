using System;
using System.Collections.Generic;
using UnityEngine;

public class SerializeDictionary
{
    public static string DicToJson<TKey, TValue>(Dictionary<TKey, TValue> dictionary, bool prettyPrint = false)
    {
        return JsonUtility.ToJson(new SerializationDictionary<TKey, TValue>(dictionary), prettyPrint);
    }

    public static Dictionary<TKey, TValue> DicFromJson<TKey, TValue>(string str)
    {
        return JsonUtility.FromJson<SerializationDictionary<TKey, TValue>>(str).ToDictionary();
    }
}

[Serializable]
public class SerializationDictionary<TKey, TValue> : ISerializationCallbackReceiver
{
    [SerializeField] private List<TKey> keys;
    [SerializeField] private List<TValue> values;

    private Dictionary<TKey, TValue> dictionary;

    public SerializationDictionary(Dictionary<TKey, TValue> dictionary)
    {
        this.dictionary = dictionary;
    }

    public Dictionary<TKey, TValue> ToDictionary() => dictionary;

    public void OnBeforeSerialize()
    {
        keys = new List<TKey>(dictionary.Keys);
        values = new List<TValue>(dictionary.Values);
    }

    public void OnAfterDeserialize()
    {
        var count = Math.Min(keys.Count, values.Count);
        dictionary = new Dictionary<TKey, TValue>(count);
        for (int i = 0; i < count; i++)
        {
            dictionary.Add(keys[i], values[i]);
        }
    }
}