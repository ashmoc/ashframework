using UnityEngine;
using UnityEngine.Serialization;
using UnityEngine.UI;

namespace AshFramework.Q
{
    public class GameStartPanel : MonoBehaviour
    {
        public GameObject enemys;

        void Start()
        {
            transform.Find("BtnStart").GetComponent<Button>().onClick
                .AddListener(() =>
                {
                    gameObject.SetActive(false);
                    enemys.SetActive(true);
                });
        }

        // Update is called once per frame
        void Update()
        {
        }
    }
}