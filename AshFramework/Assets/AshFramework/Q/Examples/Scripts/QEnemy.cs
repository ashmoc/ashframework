using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace AshFramework.Q
{
    public class QEnemy : MonoBehaviour
    {
        private static int _killedEnemyCount = 0;

        private void OnMouseDown()
        {
            _killedEnemyCount++;
            Destroy(gameObject);
            if (_killedEnemyCount >= 5)
            {
                Debug.Log("游戏通关");
            }
        }
    }
}