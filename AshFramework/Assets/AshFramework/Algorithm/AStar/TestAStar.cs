using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class TestAStar : MonoBehaviour
{
    public int beginX = -3;
    public int beginY = 5;
    public int offsetX = 2;
    public int offSetY = 2;
    public int mapW = 5;
    public int mapH = 5;

    public Material white;
    public Material red;
    public Material yellow;
    public Material green;

    private Vector2 _beginPos = Vector2.right * -1;
    private Vector2 _endPos = new Vector2();

    private List<AStarNode> _path;

    private Dictionary<string, GameObject> _cubes = new Dictionary<string, GameObject>();

    void Start()
    {
        AStarManager.Instance.InitMapInfo(mapW, mapH);
        for (int i = 0; i < mapW; i++)
        {
            for (int j = 0; j < mapH; j++)
            {
                GameObject go = GameObject.CreatePrimitive(PrimitiveType.Cube);
                go.name = $"{i}_{j}";
                go.transform.position = new Vector3(beginX + i * offsetX, beginY + j * offSetY);
                go.GetComponent<MeshRenderer>().material = white;
                _cubes.Add(go.name, go);
                AStarNode node = AStarManager.Instance.Nodes[i, j];
                if (node.Type == E_Node_Type.Stop)
                {
                    go.GetComponent<MeshRenderer>().material = red;
                }
            }
        }
    }

    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            RaycastHit info;
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            if (Physics.Raycast(ray, out info, 1000))
            {
                if (_beginPos == Vector2.right * -1)
                {
                    //清空上次路径
                    if (_path != null)
                    {
                        for (int i = 0; i < _path.Count; i++)
                        {
                            if (_cubes.ContainsKey($"{_path[i].X}_{_path[i].Y}"))
                            {
                                _cubes[$"{_path[i].X}_{_path[i].Y}"].GetComponent<MeshRenderer>().material = white;
                            }
                        }
                    }

                    string[] strs = info.collider.name.Split('_');
                    if (AStarManager.Instance.Nodes[int.Parse(strs[0]), int.Parse(strs[1])].Type != E_Node_Type.Stop)
                    {
                        _beginPos = new Vector2(int.Parse(strs[0]), int.Parse(strs[1]));
                        info.collider.gameObject.GetComponent<MeshRenderer>().material = yellow;
                    }
                }
                else
                {
                    string[] strs = info.collider.name.Split('_');
                    _endPos = new Vector2(int.Parse(strs[0]), int.Parse(strs[1]));

                    _path = AStarManager.Instance.FindPath(_beginPos, _endPos);

                    _cubes[$"{(int)_beginPos.x}_{(int)_beginPos.y}"].GetComponent<MeshRenderer>().material = white;

                    if (_path != null)
                    {
                        for (int i = 0; i < _path.Count; i++)
                        {
                            if (_cubes.ContainsKey($"{_path[i].X}_{_path[i].Y}"))
                            {
                                if (i == _path.Count - 1)
                                {
                                    _cubes[$"{_path[i].X}_{_path[i].Y}"].GetComponent<MeshRenderer>().material = yellow;
                                    break;
                                }

                                _cubes[$"{_path[i].X}_{_path[i].Y}"].GetComponent<MeshRenderer>().material = green;
                            }
                        }
                    }

                    //清除开始点
                    _beginPos = Vector2.right * -1;
                }
            }
        }
    }
}