using System.Collections.Generic;
using System.Linq;
using UnityEngine;

/// <summary>A星寻路管理器</summary>
public class AStarManager
{
    public static AStarManager Instance { get; } = new();

    /// <summary>地图的宽</summary>
    public int MapWidth;

    /// <summary>地图的高</summary>
    public int MapHeight;

    /// <summary>地图相关所有格子对象的容器</summary>
    public AStarNode[,] Nodes;

    /// <summary>开启列表</summary>
    private List<AStarNode> _openList = new List<AStarNode>();

    /// <summary>关闭列表，寻到的可到达点</summary>
    private List<AStarNode> _closeList = new List<AStarNode>();

    /// <summary>初始化地图信息</summary>
    public void InitMapInfo(int w, int h)
    {
        MapWidth = w;
        MapHeight = h;

        Nodes = new AStarNode[w, h];

        for (int i = 0; i < w; i++)
        {
            for (int j = 0; j < h; j++)
            {
                AStarNode node = new AStarNode(i, j, Random.Range(0, 100) > 20 ? E_Node_Type.Walk : E_Node_Type.Stop);
                Nodes[i, j] = node;
            }
        }
    }

    /// <summary>
    /// 寻路方法
    /// </summary>
    /// <param name="startPos">起点</param>
    /// <param name="endPos">终点</param>
    /// <returns></returns>
    public List<AStarNode> FindPath(Vector2 startPos, Vector2 endPos)
    {
        //在地图范围内
        if (startPos.x < 0 || startPos.x >= MapWidth || startPos.y < 0 || startPos.y >= MapHeight ||
            endPos.x < 0 || endPos.x >= MapWidth || endPos.y < 0 || endPos.y >= MapHeight)
        {
            Debug.Log("在地图范围外");
            return null;
        }

        //不可阻挡
        AStarNode start = Nodes[(int)startPos.x, (int)startPos.y];
        AStarNode end = Nodes[(int)endPos.x, (int)endPos.y];
        if (start.Type == E_Node_Type.Stop || end.Type == E_Node_Type.Stop)
        {
            Debug.Log("起始点或终点为不可到达点");
            return null;
        }

        //把开始点放入到关闭列表中
        _openList.Clear();
        _closeList.Clear();
        start.Father = null;
        start.F = 0;
        start.G = 0;
        start.H = 0;
        _closeList.Add(start);

        while (true)
        {
            //左上
            // FindRoundNodeToOpenList(start.X - 1, start.Y - 1, 1.4f, start, end);
            //上
            FindRoundNodeToOpenList(start.X, start.Y - 1, 1, start, end);
            //右上
            // FindRoundNodeToOpenList(start.X + 1, start.Y - 1, 1.4f, start, end);
            //左
            FindRoundNodeToOpenList(start.X - 1, start.Y, 1, start, end);
            //右
            FindRoundNodeToOpenList(start.X + 1, start.Y, 1, start, end);
            //左下
            // FindRoundNodeToOpenList(start.X - 1, start.Y + 1, 1.4f, start, end);
            //下
            FindRoundNodeToOpenList(start.X, start.Y + 1, 1, start, end);
            //右下
            // FindRoundNodeToOpenList(start.X + 1, start.Y + 1, 1.4f, start, end);

            //死路判断，当前开启列表为空。表示所有的路径都被寻找完毕
            if (_openList.Count == 0)
            {
                Debug.Log("终点不可到达");
                return null;
            }

            //选出开启列表中寻路消耗最小的那个点
            _openList.Sort((x, y) => x.F.CompareTo(y.F));

            //放入关闭列表再从开启列表中移除
            _closeList.Add(_openList[0]);
            start = _openList[0];
            _openList.RemoveAt(0);

            //找到路径
            if (start == end)
            {
                List<AStarNode> path = new List<AStarNode>();
                path.Add(end);
                while (end.Father != null)
                {
                    path.Add(end.Father);
                    end = end.Father;
                }

                path.Reverse();
                return path;
            }
        }
    }

    /// <summary>把临近的点放入开启列表</summary>
    private void FindRoundNodeToOpenList(int x, int y, float g, AStarNode father, AStarNode end)
    {
        //边界判断
        if (x < 0 || x >= MapWidth || y < 0 || y >= MapHeight) return;

        AStarNode node = Nodes[x, y];
        if (node == null || node.Type == E_Node_Type.Stop || _closeList.Contains(node) || _openList.Contains(node))
            return;

        //核心计算，计算 f 值 f = g + h
        node.Father = father;
        //计算 g ，node 离起点的距离 = father.G + g ，有一个累加过程
        node.G = father.G + g;
        node.H = Mathf.Abs(end.X - node.X) + Mathf.Abs(end.Y - node.Y);
        node.F = node.G + node.H;

        _openList.Add(node);
    }
}