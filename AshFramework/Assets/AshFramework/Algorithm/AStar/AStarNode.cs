
public enum E_Node_Type
{
    /// <summary>可以走的地方</summary>
    Walk,
    /// <summary>不能走的阻挡</summary>
    Stop
}

/// <summary>A星格子类</summary>
public class AStarNode
{
    //格子对象的坐标
    public int X;
    public int Y;

    /// <summary>寻路消耗</summary>
    public float F;
    /// <summary>离起点的距离</summary>
    public float G;
    /// <summary>离终点的距离-曼哈顿距离</summary>
    public float H;

    /// <summary>父对象</summary>
    public AStarNode Father;
    
    /// <summary>格子的类型</summary>
    public E_Node_Type Type;

    /// <summary>
    /// 格子类的构造函数
    /// </summary>
    /// <param name="x">坐标 x</param>
    /// <param name="y">坐标 y</param>
    /// <param name="type">格子类型</param>
    public AStarNode(int x, int y, E_Node_Type type)
    {
        X = x;
        Y = y;
        Type = type;
    }
}
