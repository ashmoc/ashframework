using System;
using System.Collections;
using System.Timers;
using UnityEngine;

namespace AshFramework
{
    public class Example : MonoBehaviour
    {
        private ResLoader _resLoader;

        private void Start()
        {
            _resLoader = new ResLoader();

            var go = _resLoader.LoadSyncFromAssetBundle<GameObject>("plane", "Plane");
            Instantiate(go);
        }

        private void OnDestroy()
        {
            _resLoader.ReleaseAll();
            _resLoader = null;
        }
    }
}